import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ApolloClient from "apollo-boost";
import { ApolloProvider, Query } from "react-apollo";
import gql from 'graphql-tag';

const client = new ApolloClient({
  uri: "https://localhost:4000/graphql",
});

const query = gql`
  {
    getAllIngredients{
      name
    }
  }
`

const Teste = (props) => (
 <Query query={query} >
    {({data, loading}) => (
      loading ?
        <h1>teste</h1>
      :
      <div>
        {data.getAllIngredients}
      </div>
    )}
 </Query>
);


class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
          </header>
          <p className="App-intro">
            <Teste/>
          </p>
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
